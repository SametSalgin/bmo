﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;



namespace WindowsFormsApp12
{
    public partial class Form1 : Form
    {
        dosyalama dosya = new dosyalama();
        listview cizelge = new listview();
        string dosyaYolu = "",strcinsiyet = "";
        string[] strbilgiler;
        int index;
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

            cizelge.listview_baslıklar(ref listViewBmo);
            dosya.dosyaOkuma( ref listViewBmo);
           
        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            
            bool boolkontrol = false;
            if(radioErkek.Checked==true)
            {
                strcinsiyet = radioErkek.Text;
            }
            else
            {
                strcinsiyet = radioKadın.Text;
            }
            string[] strbilgiler = { txtTc.Text, txtIsim.Text, txtSoyisim.Text, txtAdres.Text, txtMaas.Text,strcinsiyet,
                                    cmbsehir.Text, cmbYoneticilik.Text, cmbdeneyim.Text, cmbYabancidil.Text,cmbAkedemik.Text,cmbAileDurumu.Text };


           
            for (int i = 0; i < listViewBmo.Items.Count; i++)
            {
                if(listViewBmo.Items[i].SubItems[0].Text==txtTc.Text)
                {
                    boolkontrol = true;
                    MessageBox.Show(txtTc.Text + " TC NO başkasına ait");
                }
            }

            if (boolkontrol == false)
            {
                ListViewItem eleman = new ListViewItem(strbilgiler);
                listViewBmo.Items.Add(eleman);
                text_bosaltma();
            }
        }
        private void btnSil_Click(object sender, EventArgs e)
        {
            cizelge.listview_sil(ref listViewBmo);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            dosya.dosyaYazma(ref listViewBmo);
        }

        private void listViewBmo_ItemChecked(object sender, ItemCheckedEventArgs e)
        {

            if (listViewBmo.CheckedItems.Count >0)
            {

                ListViewItem secili = listViewBmo.CheckedItems[listViewBmo.CheckedItems.Count-1];
                index = listViewBmo.Items.IndexOf(listViewBmo.CheckedItems[listViewBmo.CheckedItems.Count-1 ]);
                txtTc.Text = secili.SubItems[0].Text;
                txtIsim.Text = secili.SubItems[1].Text;
                txtSoyisim.Text = secili.SubItems[2].Text;
                txtAdres.Text = secili.SubItems[3].Text;
                if (secili.SubItems[4].Text == "ERKEK")
                    radioErkek.Checked = true;
                else
                    radioKadın.Checked = true;
                cmbsehir.Text = secili.SubItems[6].Text;
                cmbYoneticilik.Text = secili.SubItems[7].Text;
                cmbdeneyim.Text = secili.SubItems[8].Text;
                cmbYabancidil.Text = secili.SubItems[9].Text;
                cmbAkedemik.Text = secili.SubItems[10].Text;
                cmbAileDurumu.Text = secili.SubItems[11].Text;

            }
            else
            {
                index = -1;
            }
        }
        private void text_bosaltma()
        {
            txtTc.Text = "";
            txtIsim.Text = "";
            txtSoyisim.Text = "";
            txtAdres.Text = "";

        }


        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            if (index == -1)
            {
                MessageBox.Show("SEÇİLİ KİŞİ YOK", "HATA!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                if (radioErkek.Checked == true)
                {
                    strcinsiyet = radioErkek.Text;
                }
                else
                {
                    strcinsiyet = radioKadın.Text;
                }
                string[] strbilgiler = { txtTc.Text, txtIsim.Text, txtSoyisim.Text, txtAdres.Text, txtMaas.Text,strcinsiyet,
                                    cmbsehir.Text, cmbYoneticilik.Text, cmbdeneyim.Text, cmbYabancidil.Text,cmbAkedemik.Text,cmbAileDurumu.Text };
                ListViewItem eleman = new ListViewItem(strbilgiler);
                listViewBmo.Items.RemoveAt(index);
                listViewBmo.Items.Insert(index, eleman);
                text_bosaltma();
            }
        }
        
    }
}

