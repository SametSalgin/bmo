﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
namespace WindowsFormsApp12
{
    class dosyalama
    {
        string dosyayolu = @"C:\Users\samet\source\repos\WindowsFormsApp12\WindowsFormsApp12\BMO.txt";

        public void dosyaOkuma( ref ListView listViewBmo)
        {
            int i = 0;
            OpenFileDialog dosyaAcma = new OpenFileDialog();
            dosyaAcma.Filter = "Text File | *.txt";
            if (File.Exists(dosyayolu))
            {
                String line = "";
                using (StreamReader sr = new StreamReader(dosyayolu))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] strdiziBMO = line.Split(',');
                        string[] bmoPersonelBilgi = new string[strdiziBMO.Length];
                        foreach (var sozcuk in strdiziBMO)
                        {
                            bmoPersonelBilgi[i] = sozcuk;
                            i++;
                        }
                        i = 0;
                        ListViewItem eleman1 = new ListViewItem(bmoPersonelBilgi);
                        listViewBmo.Items.Add(eleman1);
                    }
                }
            }
            else
            {
                SaveFileDialog dosyaYazma = new SaveFileDialog();
                dosyaYazma.FileName = "BMO.txt";
                dosyaYazma.Filter = "Text File | *.txt";
                
                    StreamWriter yazici = new StreamWriter(dosyayolu);
                    yazici.Dispose();
                    yazici.Close();
                    
            }
        }
        public void dosyaYazma( ref ListView listViewBmo)
        {

            ListViewItem.ListViewSubItem listViewSubItem = new ListViewItem.ListViewSubItem();
            ListViewItem lvi = new ListViewItem();
            using (StreamWriter yazici = new StreamWriter("C:\\Users\\samet\\source\\repos\\WindowsFormsApp12\\WindowsFormsApp12\\BMO.txt"))
            {
                if (listViewBmo.Items.Count > 0)
                {
                    StringBuilder stringyapici = new StringBuilder();
                    for (int j = 0; j < listViewBmo.Items.Count; j++)
                    {
                        lvi = listViewBmo.Items[j];
                        stringyapici = new StringBuilder();
                        for (int i = 0; i < lvi.SubItems.Count; i++)
                        {
                            listViewSubItem = lvi.SubItems[i];
                            if (i < 11)
                                stringyapici.Append(string.Format("{0},", listViewSubItem.Text));
                            if (i == 11)
                                stringyapici.Append(string.Format("{0}", listViewSubItem.Text));
                        }
                        if (j < listViewBmo.Items.Count - 1)
                            yazici.WriteLine(stringyapici.ToString());
                        if (j == listViewBmo.Items.Count - 1)
                            yazici.Write(stringyapici.ToString());
                    }
                    yazici.WriteLine();
                    yazici.Close();
                }
            }
        }
    }
}
