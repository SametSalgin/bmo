﻿namespace WindowsFormsApp12
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEkle = new System.Windows.Forms.Button();
            this.btnGuncelle = new System.Windows.Forms.Button();
            this.btnSil = new System.Windows.Forms.Button();
            this.cmbdeneyim = new System.Windows.Forms.ComboBox();
            this.cmbsehir = new System.Windows.Forms.ComboBox();
            this.cmbAkedemik = new System.Windows.Forms.ComboBox();
            this.cmbYabancidil = new System.Windows.Forms.ComboBox();
            this.cmbYoneticilik = new System.Windows.Forms.ComboBox();
            this.cmbAileDurumu = new System.Windows.Forms.ComboBox();
            this.txtIsim = new System.Windows.Forms.TextBox();
            this.txtSoyisim = new System.Windows.Forms.TextBox();
            this.txtAdres = new System.Windows.Forms.TextBox();
            this.txtTc = new System.Windows.Forms.TextBox();
            this.radioErkek = new System.Windows.Forms.RadioButton();
            this.radioKadın = new System.Windows.Forms.RadioButton();
            this.listViewBmo = new System.Windows.Forms.ListView();
            this.txtMaas = new System.Windows.Forms.TextBox();
            this.lblIsim = new System.Windows.Forms.Label();
            this.lblAdres = new System.Windows.Forms.Label();
            this.lbSoyisim = new System.Windows.Forms.Label();
            this.lbltc = new System.Windows.Forms.Label();
            this.lblMaas = new System.Windows.Forms.Label();
            this.grpBoxCinsiyet = new System.Windows.Forms.GroupBox();
            this.grpBoxCinsiyet.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEkle
            // 
            this.btnEkle.Location = new System.Drawing.Point(602, 18);
            this.btnEkle.Name = "btnEkle";
            this.btnEkle.Size = new System.Drawing.Size(121, 34);
            this.btnEkle.TabIndex = 1;
            this.btnEkle.Text = "EKLE";
            this.btnEkle.UseVisualStyleBackColor = true;
            this.btnEkle.Click += new System.EventHandler(this.btnEkle_Click);
            // 
            // btnGuncelle
            // 
            this.btnGuncelle.Location = new System.Drawing.Point(602, 79);
            this.btnGuncelle.Name = "btnGuncelle";
            this.btnGuncelle.Size = new System.Drawing.Size(121, 34);
            this.btnGuncelle.TabIndex = 2;
            this.btnGuncelle.Text = "GÜNCELLE";
            this.btnGuncelle.UseVisualStyleBackColor = true;
            this.btnGuncelle.Click += new System.EventHandler(this.btnGuncelle_Click);
            // 
            // btnSil
            // 
            this.btnSil.Location = new System.Drawing.Point(602, 136);
            this.btnSil.Name = "btnSil";
            this.btnSil.Size = new System.Drawing.Size(121, 34);
            this.btnSil.TabIndex = 3;
            this.btnSil.Text = "SİL";
            this.btnSil.UseVisualStyleBackColor = true;
            this.btnSil.Click += new System.EventHandler(this.btnSil_Click);
            // 
            // cmbdeneyim
            // 
            this.cmbdeneyim.FormattingEnabled = true;
            this.cmbdeneyim.Items.AddRange(new object[] {
            "0-2",
            "2-4",
            "5-9",
            "10-14",
            "15-20",
            "20 VE ÜZERİ"});
            this.cmbdeneyim.Location = new System.Drawing.Point(271, 92);
            this.cmbdeneyim.Name = "cmbdeneyim";
            this.cmbdeneyim.Size = new System.Drawing.Size(290, 21);
            this.cmbdeneyim.TabIndex = 17;
            this.cmbdeneyim.Text = "DENEYİM";
            // 
            // cmbsehir
            // 
            this.cmbsehir.FormattingEnabled = true;
            this.cmbsehir.Items.AddRange(new object[] {
            "TR10: İstanbul ",
            "TR51: Ankara",
            "TR31: İzmir",
            "TR42: Kocaeli, Sakarya, Düzce, Bolu, Yalova",
            "TR21: Edirne, Kırklareli, Tekirdağ",
            "TR90: Trabzon, Ordu, Giresun, Rize, Artvin, Gümüşhane",
            "TR41: Bursa, Eskişehir, Bilecik ",
            "TR32: Aydın, Denizli, Muğla",
            "TR62: Adana, Mersin",
            "TR22: Balıkesir, Çanakkale",
            "TR61: Antalya, Isparta, Burdur",
            "Diğer İller "});
            this.cmbsehir.Location = new System.Drawing.Point(271, 10);
            this.cmbsehir.Name = "cmbsehir";
            this.cmbsehir.Size = new System.Drawing.Size(290, 21);
            this.cmbsehir.TabIndex = 18;
            this.cmbsehir.Text = "ŞEHİR";
            // 
            // cmbAkedemik
            // 
            this.cmbAkedemik.FormattingEnabled = true;
            this.cmbAkedemik.Items.AddRange(new object[] {
            "Meslek alanı ile ilgili yüksek lisans",
            "Meslek alanı ile ilgili doktora",
            "Meslek alanı ile ilgili doçentlik",
            "Meslek alanı ile ilgili olmayan yüksek lisans",
            "Meslek alanı ile ilgili olmayan doktora/doçentlik"});
            this.cmbAkedemik.Location = new System.Drawing.Point(271, 180);
            this.cmbAkedemik.Name = "cmbAkedemik";
            this.cmbAkedemik.Size = new System.Drawing.Size(290, 21);
            this.cmbAkedemik.TabIndex = 19;
            this.cmbAkedemik.Text = "AKEDEMİK BİLGİ";
            // 
            // cmbYabancidil
            // 
            this.cmbYabancidil.FormattingEnabled = true;
            this.cmbYabancidil.Items.AddRange(new object[] {
            "Belgelendirilmiş İngilizce bilgisi ",
            "İngilizce eğitim veren okul mezuniyeti",
            "Belgelendirilmiş diğer yabancı dil bilgisi (her dil için)"});
            this.cmbYabancidil.Location = new System.Drawing.Point(271, 136);
            this.cmbYabancidil.Name = "cmbYabancidil";
            this.cmbYabancidil.Size = new System.Drawing.Size(290, 21);
            this.cmbYabancidil.TabIndex = 20;
            this.cmbYabancidil.Text = "YABANCI DİL";
            // 
            // cmbYoneticilik
            // 
            this.cmbYoneticilik.FormattingEnabled = true;
            this.cmbYoneticilik.Items.AddRange(new object[] {
            "Takım Lideri/Grup Yöneticisi/Teknik Yönetici/Yazılım Mimarı",
            "Proje Yöneticisi",
            "Direktör/Projeler Yöneticisi ",
            "CTO/Genel Müdür",
            "Bilgi İşlem Sorumlusu/Müdürü (en çok 5)",
            "Bilgi İşlem Sorumlusu/Müdürü (en az 5)"});
            this.cmbYoneticilik.Location = new System.Drawing.Point(271, 52);
            this.cmbYoneticilik.Name = "cmbYoneticilik";
            this.cmbYoneticilik.Size = new System.Drawing.Size(290, 21);
            this.cmbYoneticilik.TabIndex = 21;
            this.cmbYoneticilik.Text = "YÖNETİCİLİK GÖREVİ";
            // 
            // cmbAileDurumu
            // 
            this.cmbAileDurumu.FormattingEnabled = true;
            this.cmbAileDurumu.Items.AddRange(new object[] {
            "Evli ve eşi çalışmıyor",
            "0-6 yaş arası çocuk ",
            "7-18 yaş arası çocu",
            "18 yaş üstü çocuk"});
            this.cmbAileDurumu.Location = new System.Drawing.Point(271, 219);
            this.cmbAileDurumu.Name = "cmbAileDurumu";
            this.cmbAileDurumu.Size = new System.Drawing.Size(290, 21);
            this.cmbAileDurumu.TabIndex = 22;
            this.cmbAileDurumu.Text = "AİLE DURUMU";
            // 
            // txtIsim
            // 
            this.txtIsim.Location = new System.Drawing.Point(78, 52);
            this.txtIsim.Name = "txtIsim";
            this.txtIsim.Size = new System.Drawing.Size(161, 20);
            this.txtIsim.TabIndex = 23;
            // 
            // txtSoyisim
            // 
            this.txtSoyisim.Location = new System.Drawing.Point(78, 93);
            this.txtSoyisim.Name = "txtSoyisim";
            this.txtSoyisim.Size = new System.Drawing.Size(161, 20);
            this.txtSoyisim.TabIndex = 24;
            // 
            // txtAdres
            // 
            this.txtAdres.Location = new System.Drawing.Point(78, 137);
            this.txtAdres.Name = "txtAdres";
            this.txtAdres.Size = new System.Drawing.Size(161, 20);
            this.txtAdres.TabIndex = 25;
            // 
            // txtTc
            // 
            this.txtTc.Location = new System.Drawing.Point(78, 11);
            this.txtTc.Name = "txtTc";
            this.txtTc.Size = new System.Drawing.Size(161, 20);
            this.txtTc.TabIndex = 26;
            // 
            // radioErkek
            // 
            this.radioErkek.AutoSize = true;
            this.radioErkek.Checked = true;
            this.radioErkek.Location = new System.Drawing.Point(6, 18);
            this.radioErkek.Name = "radioErkek";
            this.radioErkek.Size = new System.Drawing.Size(61, 17);
            this.radioErkek.TabIndex = 28;
            this.radioErkek.TabStop = true;
            this.radioErkek.Text = "ERKEK";
            this.radioErkek.UseVisualStyleBackColor = true;
            // 
            // radioKadın
            // 
            this.radioKadın.AutoSize = true;
            this.radioKadın.Location = new System.Drawing.Point(136, 18);
            this.radioKadın.Name = "radioKadın";
            this.radioKadın.Size = new System.Drawing.Size(58, 17);
            this.radioKadın.TabIndex = 29;
            this.radioKadın.Text = "KADIN";
            this.radioKadın.UseVisualStyleBackColor = true;
            // 
            // listViewBmo
            // 
            this.listViewBmo.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.listViewBmo.CheckBoxes = true;
            this.listViewBmo.GridLines = true;
            this.listViewBmo.Location = new System.Drawing.Point(12, 277);
            this.listViewBmo.Name = "listViewBmo";
            this.listViewBmo.Size = new System.Drawing.Size(1070, 275);
            this.listViewBmo.TabIndex = 30;
            this.listViewBmo.UseCompatibleStateImageBehavior = false;
            this.listViewBmo.View = System.Windows.Forms.View.Details;
            this.listViewBmo.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewBmo_ItemChecked);
            // 
            // txtMaas
            // 
            this.txtMaas.Enabled = false;
            this.txtMaas.Location = new System.Drawing.Point(78, 181);
            this.txtMaas.Name = "txtMaas";
            this.txtMaas.Size = new System.Drawing.Size(161, 20);
            this.txtMaas.TabIndex = 31;
            this.txtMaas.Text = "0";
            // 
            // lblIsim
            // 
            this.lblIsim.AutoSize = true;
            this.lblIsim.Location = new System.Drawing.Point(13, 60);
            this.lblIsim.Name = "lblIsim";
            this.lblIsim.Size = new System.Drawing.Size(29, 13);
            this.lblIsim.TabIndex = 32;
            this.lblIsim.Text = "İSİM";
            // 
            // lblAdres
            // 
            this.lblAdres.AutoSize = true;
            this.lblAdres.Location = new System.Drawing.Point(13, 144);
            this.lblAdres.Name = "lblAdres";
            this.lblAdres.Size = new System.Drawing.Size(44, 13);
            this.lblAdres.TabIndex = 33;
            this.lblAdres.Text = "ADRES";
            // 
            // lbSoyisim
            // 
            this.lbSoyisim.AutoSize = true;
            this.lbSoyisim.Location = new System.Drawing.Point(13, 100);
            this.lbSoyisim.Name = "lbSoyisim";
            this.lbSoyisim.Size = new System.Drawing.Size(51, 13);
            this.lbSoyisim.TabIndex = 34;
            this.lbSoyisim.Text = "SOYİSİM";
            // 
            // lbltc
            // 
            this.lbltc.AutoSize = true;
            this.lbltc.Location = new System.Drawing.Point(13, 18);
            this.lbltc.Name = "lbltc";
            this.lbltc.Size = new System.Drawing.Size(40, 13);
            this.lbltc.TabIndex = 35;
            this.lbltc.Text = "TC NO";
            // 
            // lblMaas
            // 
            this.lblMaas.AutoSize = true;
            this.lblMaas.Location = new System.Drawing.Point(13, 188);
            this.lblMaas.Name = "lblMaas";
            this.lblMaas.Size = new System.Drawing.Size(37, 13);
            this.lblMaas.TabIndex = 36;
            this.lblMaas.Text = "MAAŞ";
            // 
            // grpBoxCinsiyet
            // 
            this.grpBoxCinsiyet.Controls.Add(this.radioErkek);
            this.grpBoxCinsiyet.Controls.Add(this.radioKadın);
            this.grpBoxCinsiyet.Location = new System.Drawing.Point(39, 207);
            this.grpBoxCinsiyet.Name = "grpBoxCinsiyet";
            this.grpBoxCinsiyet.Size = new System.Drawing.Size(200, 45);
            this.grpBoxCinsiyet.TabIndex = 37;
            this.grpBoxCinsiyet.TabStop = false;
            this.grpBoxCinsiyet.Text = "CİNSİYET";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 564);
            this.Controls.Add(this.grpBoxCinsiyet);
            this.Controls.Add(this.lblMaas);
            this.Controls.Add(this.lbltc);
            this.Controls.Add(this.lbSoyisim);
            this.Controls.Add(this.lblAdres);
            this.Controls.Add(this.lblIsim);
            this.Controls.Add(this.txtMaas);
            this.Controls.Add(this.listViewBmo);
            this.Controls.Add(this.txtTc);
            this.Controls.Add(this.txtAdres);
            this.Controls.Add(this.txtSoyisim);
            this.Controls.Add(this.txtIsim);
            this.Controls.Add(this.cmbAileDurumu);
            this.Controls.Add(this.cmbYoneticilik);
            this.Controls.Add(this.cmbYabancidil);
            this.Controls.Add(this.cmbAkedemik);
            this.Controls.Add(this.cmbsehir);
            this.Controls.Add(this.cmbdeneyim);
            this.Controls.Add(this.btnSil);
            this.Controls.Add(this.btnGuncelle);
            this.Controls.Add(this.btnEkle);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpBoxCinsiyet.ResumeLayout(false);
            this.grpBoxCinsiyet.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnEkle;
        private System.Windows.Forms.Button btnGuncelle;
        private System.Windows.Forms.Button btnSil;
        private System.Windows.Forms.ComboBox cmbdeneyim;
        private System.Windows.Forms.ComboBox cmbsehir;
        private System.Windows.Forms.ComboBox cmbAkedemik;
        private System.Windows.Forms.ComboBox cmbYabancidil;
        private System.Windows.Forms.ComboBox cmbYoneticilik;
        private System.Windows.Forms.ComboBox cmbAileDurumu;
        private System.Windows.Forms.TextBox txtIsim;
        private System.Windows.Forms.TextBox txtSoyisim;
        private System.Windows.Forms.TextBox txtAdres;
        private System.Windows.Forms.TextBox txtTc;
        private System.Windows.Forms.RadioButton radioErkek;
        private System.Windows.Forms.RadioButton radioKadın;
        private System.Windows.Forms.ListView listViewBmo;
        private System.Windows.Forms.TextBox txtMaas;
        private System.Windows.Forms.Label lblIsim;
        private System.Windows.Forms.Label lblAdres;
        private System.Windows.Forms.Label lbSoyisim;
        private System.Windows.Forms.Label lbltc;
        private System.Windows.Forms.Label lblMaas;
        private System.Windows.Forms.GroupBox grpBoxCinsiyet;
    }
}

